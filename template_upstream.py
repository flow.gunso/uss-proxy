#!/usr/bin/env python
# -*- coding: utf-8 -*-

from string import Template
import os

# Read the application socket filename from environment variable.
if 'APP_SOCKET_FILENAME' in os.environ:
    app_socket_filename = os.environ['APP_SOCKET_FILENAME']
else:
    app_socket_filename = 'app.sock'

# Set subtitutes.
substitutes = {'app_socket_filename':app_socket_filename}

# Read, set the template.
with open('upstream.conf', 'r') as f:
    t = Template(f.read())

# Overwrite with substitutes.
with open('upstream.conf', 'w') as f:
    f.write(t.substitute(substitutes))