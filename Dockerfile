FROM nginx:1.15.7-alpine

RUN apk add python

COPY nginx.conf /etc/nginx/nginx.conf
COPY upstream.conf /etc/nginx/upstream.conf
COPY template_upstream.py /etc/nginx/template_upstream.py
COPY entrypoint.sh /

WORKDIR /etc/nginx

ENTRYPOINT [ "/bin/sh", "/entrypoint.sh" ]