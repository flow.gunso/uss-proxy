# CHANGELOG

# [1.1] - 2018/12/03
- Add the option to name the application socket to proxy, while defaulting to `app.sock`:
  - Add templating the upstream configuration.
  - Override the default entrypoint

# [1.0] - 2018/12/01
- Initial release