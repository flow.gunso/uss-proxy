# USS Proxy - Unix Socket & Statics Proxy

[![pipeline status](https://gitlab.com/flow.gunso/uss-proxy/badges/master/pipeline.svg)](https://gitlab.com/flow.gunso/uss-proxy/commits/master)

Docker image that act as a proxy to statics files and a unix socket backend.
Used in docker-compose for deploying application into production.

# Usage

Let's get directly to the point with a very short `docker-compose.yml` example:
```
version: '3'

services:
  uss-proxy:
    image: registry.gitlab.com/flow.gunso/uss-proxy:latest
    volumes:
      - runtimes:/var/run/uss
      - statics:/var/www/uss/statics
    environment:
      - "APP_SOCKET_FILENAME=foo.sock" # Set this to your-app socket filename. Defaults to `app.sock`.

  your-app:
    image: your-app:latest
    volumes:
      - runtimes:/path/to/your/app/socket
      - statics:/path/to/your/app/static/files

volumes:
  runtimes:
  statics:
```

Let's assume that `your-app` contain a Django appplication served with Gunicorn with the following:

`YourApp/settings.py`:
```
STATIC_ROOT = os.path.join('/', 'path', 'to', 'your', 'app', 'static', 'files')
STATIC_URL = '/static/
```
`entrypoint.sh`
```
gunicorn YourApp.wsgi -b unix:/path/to/your/app/socket/foo.sock
```

__USS Proxy__ will pass all requests to the Unix domain socket named `foo.sock` located in it's own directory `/var/run/uss/`, except for request starting with `/static/` which it will pass to it's own directory `/var/www/uss/statics/`. 

In essence, that's it.

The rest is the application settings, the service stack configuration and probably other things I'm not naming, but that's your job.

You check [this snippet](https://gitlab.com/flow.gunso/pinlist/snippets/1786316) from [PinList](https://gitlab.com/flow.gunso/pinlist) for a real word use case with Traefik.

# TODO

- Write the rest of this document